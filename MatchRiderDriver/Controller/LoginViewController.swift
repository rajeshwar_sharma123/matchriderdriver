//
//  LoginViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 07/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var baseView: UIView!
    @IBOutlet var logoImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.bringSubview(toFront: baseView)
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        self.view.layoutIfNeeded()
        let facebookButton = MyFBLoginButton()
        facebookButton.readPermissions = ["email"]
        facebookButton.loginBehavior = .web
        facebookButton.delegate = self
        facebookButton.frame =  buttonView.bounds
        buttonView.addSubview(facebookButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        emailTextField.text = Store.sharedStore.email
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:- Custom method action
    
    @IBAction func registerButtonAction(_ sender: AnyObject) {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        if NetworkReachability.isConnectedToNetwork(){
            loginUser()
        }else{
           self.showErrorMessage(message: NoNetworkError)
        }
        
    }
    
    //MARK:- Service call
    
    func loginUser(){
        if !Validation.validateSignIn(emailTextField.text!, password: passwordTextField.text!){
            return
        }
        self.view.showLoader()
        let params: [String : Any] = [
            "email": emailTextField.text!,
            "password": passwordTextField.text!
        ]
        HttpRequest.sharedRequest.performPostRequest( requestParam:params, endPoint: Endpoint.Login) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json == nil{
                self.view.hideLoader()
                return
            }
            let status = json["Status"].string
            if status == HttpRequest.Status.Success{
                Store.sharedStore.email = self.emailTextField.text
                let token = Token(json: (json["Payload"].array?.first)!)
                Store.sharedStore.token = token
                self.getAccountDetails()
            }else{
                self.view.hideLoader()
                self.showErrorMessage(message: LoginError)
            }
        }
    }
    
    func registerFCMDeviceTokenToServer(){
        if let deviceTokenId = FIRInstanceID.instanceID().token() {
            let params: [String : Any] = [
              "deviceId": deviceTokenId
            ]
            HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.updateDeviceFCMToken           ) { payload in
                guard let data = payload else{
                    self.view.hideLoader()
                    return
                }
                let json = HttpRequest.sharedRequest.parseData(data: data)
                let status = json["Status"].string
                if status == HttpRequest.Status.Success{
                }
                self.view.hideLoader()
            }
        }
    }
    
    func getAccountDetails(){
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token, endPoint: Endpoint.getAccountDetails) { (data) in
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                self.registerFCMDeviceTokenToServer()
                Utilities.saveUserData(user: data)
                Utilities.navigateToViewController(viewController: "GPSTrackingViewController")
            }
            self.view.hideLoader()
        }
    }
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard result?.token?.tokenString != nil else
        {
            return
        }
        self.view.showLoader()
        self.callApiToLogInUser(tokenId: result!.token!.tokenString!)
    }
    
    func callApiToLogInUser(tokenId: String){
        let params = [
            "tokenId": tokenId,
            ]
        HttpRequest.sharedRequest.performPostRequest( requestParam:params, endPoint: Endpoint.FaceBook) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let token = Token(json: (json["Payload"].array?.first)!)
                Store.sharedStore.token = token
                self.getAccountDetails()
            }
            else{
                self.view.hideLoader()
                self.showErrorMessage(message: LoginError)
            }
         }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
        
}

import FBSDKLoginKit

class MyFBLoginButton: FBSDKLoginButton {
    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle("continue with Facebook", for: state)
    }
}
