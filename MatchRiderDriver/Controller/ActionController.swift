
import UIKit

func ActionControllerNoNetwork() -> UIAlertController {
  let alert = UIAlertController(title: "Keine Internetverbindung", message: nil, preferredStyle: .alert)
  let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
  alert.addAction(ok)
  return alert
}

func ActionControllerInvalidCredentials() -> UIAlertController {
  let alert = UIAlertController(title: "Login fehlgeschlagen", message: "Deine E-Mail oder das Passwort war nicht korrekt. Bitte versuche es noch einmal.", preferredStyle: .alert)
  let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
  alert.addAction(ok)
  return alert
}

func ActionControllerServerConnection(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Verbindungsfehler", message: "Es konnte keine Verbindung mit MatchRiderGO aufgebaut werden.", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let retry = UIAlertAction(title: "Erneut versuchen", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(retry)
  return alert
}

func ActionCallImpossible() -> UIAlertController {
  let alert = UIAlertController(title: "Anruf nicht möglich", message: nil, preferredStyle: .alert)
  let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
  alert.addAction(ok)
  return alert
}

func ActionControllerCancelRide(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Fahrt stornieren", message: "Willst Du die Fahrt stornieren?", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let retry = UIAlertAction(title: "Stornieren", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(retry)
  return alert
}

func ActionControllerBankAccount(_ success: Bool, callback: @escaping () -> Void) -> UIAlertController {
  let alert: UIAlertController
  if !success {
    alert = UIAlertController(title: "Erstellung fehlgeschlagen", message: "Deine Kontodaten konnten nicht bestätigt werden.", preferredStyle: .alert)
  } else {
    alert = UIAlertController(title: "Erstellung erfolgreich", message: "Deine Kontodaten wurden bestätigt.", preferredStyle: .alert)
  }
  let ok = UIAlertAction(title: "Ok", style: .default) { action in
    callback()
  }
  alert.addAction(ok)
  return alert
}

func ActionControllerRegister(_ success: Bool, callback: @escaping () -> Void) -> UIAlertController {
  let alert: UIAlertController
  if success {
    alert = UIAlertController(title: "Anmeldung erfolgreich", message: "Dein Konto wurde erfolgreich eingerichtet.", preferredStyle: .alert)
  } else {
    alert = UIAlertController(title: "Anmeldung fehlgeschlagen", message: "Dein Konto konnte nicht erstellt werden.", preferredStyle: .alert)
  }
  let ok = UIAlertAction(title: "Ok", style: .default) { action in
    callback()
  }
  alert.addAction(ok)
  return alert
}

func ActionControllerWrongDirection(_ callback: @escaping () -> Void) -> UIAlertController {
    //Willst Du die Fahrtrichtung ändern?
  let alert = UIAlertController(title: "Falsche Richtung", message: "You can not choose match points in this direction.", preferredStyle: .alert)
  //let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let ok = UIAlertAction(title: "Ok", style: .default) { action in
    callback()
  }
  //alert.addAction(cancel)
  alert.addAction(ok)
  return alert
}


func ActionControllerBookRide(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Fahrt buchen", message: "Willst Du die Fahrt buchen?", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let book = UIAlertAction(title: "Buchen", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(book)
  return alert
}

func ActionControllerRedeemFailed() -> UIAlertController {
  let alert = UIAlertController(title: nil, message: "Gutschein konnte nicht verbucht werden.", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  alert.addAction(cancel)
  return alert
}


func ActionControllerBookingFailed() -> UIAlertController {
  let alert = UIAlertController(title: "Buchung fehlgeschlagen", message: "Es ist leider nicht möglich diese Fahrt zu buchen.", preferredStyle: .alert)
  let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
  alert.addAction(ok)
  return alert
}

func ActionControllerPaymentMissing(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Zahlungsdaten hinzufügen", message: "Zahlungsdaten werden benötigt.", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let add = UIAlertAction(title: "Ok", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(add)
  return alert
}

func ActionControllerResetPassword(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Passwort zurücksetzen", message: "Willst Du dein Passwort zurücksetzen?", preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let reset = UIAlertAction(title: "Zurücksetzen", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(reset)
  return alert
}
func ActionControllerRedeem(_ callback: @escaping () -> Void) -> UIAlertController {
  let alert = UIAlertController(title: "Gutschein einlösen", message: nil, preferredStyle: .alert)
  let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
  let reset = UIAlertAction(title: "Einlösen", style: .default) { action in
    callback()
  }
  alert.addAction(cancel)
  alert.addAction(reset)
  return alert
}
func ActionControllerCommonAlert(_ message: String? = "") -> UIAlertController {
    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alert.addAction(ok)
    return alert
}


func ActionControllerImpersonate(_ callback: @escaping () -> Void) -> UIAlertController {
    let alert = UIAlertController(title: "Imitieren", message: nil, preferredStyle: .alert)
    let cancel = UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil)
    let reset = UIAlertAction(title: "Einreichen", style: .default) { action in
        callback()
    }
    alert.addAction(cancel)
    alert.addAction(reset)
    return alert
}

