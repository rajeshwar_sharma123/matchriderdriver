//
//  FAQViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/03/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class FAQViewController: BaseViewController, UIWebViewDelegate{
    
@IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FAQ"
        webView.delegate = self
        webView.scalesPageToFit = true
        webView.contentMode = .scaleAspectFit
        self.view.showLoader()
        setBackButton()
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "FAQ", ofType: "docx")!)))
    }

    //MARK:- UIWebViewDelegate Method
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.view.hideLoader()
    }
}
