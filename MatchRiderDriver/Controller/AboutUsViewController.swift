//
//  AboutUsViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 01/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        self.title = "Über uns"
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "about_team", ofType: "html")!)))
    }
  }
