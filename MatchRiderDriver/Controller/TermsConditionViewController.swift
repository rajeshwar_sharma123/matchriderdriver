//
//  TermsConditionViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 25/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class TermsConditionViewController: BaseViewController,UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        self.view.showLoader()
        setBackButton()
        self.title = "Allgemeine Geschäftsbedingungen"
         webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "AGB", ofType: "html")!)))
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.view.hideLoader()
    }
}
