//
//  DataProtectionViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class DataProtectionViewController: BaseViewController {
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Datenschutz"
        setBackButton()
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "data_protection", ofType: "html")!)))
    }
 }
