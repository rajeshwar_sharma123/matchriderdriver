//
//  PassengerProfileViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 10/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class PassengerProfileViewController: BaseViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var messageLabel: UILabel!

    var passenger: PassengerListModel!
    var upcomingRide: Ride!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Passagier"
        setBackButton()
        registerCellNibWithTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        messageLabel.isUserInteractionEnabled = true
        messageLabel.alpha = 1.0
        
        let viewController = appDelegate.window!.rootViewController
        viewController?.menuContainerViewController.panMode = MFSideMenuPanModeNone
    }
    
    func registerCellNibWithTable(){
        tableView.register(UINib(nibName: "AcceptRejectCell", bundle: nil), forCellReuseIdentifier: "AcceptRejectCell")
        tableView.register(UINib(nibName: "AcceptCell", bundle: nil), forCellReuseIdentifier: "AcceptCell")
        tableView.register(UINib(nibName: "RejectCell", bundle: nil), forCellReuseIdentifier: "RejectCell")
        tableView.register(UINib(nibName: "PassengerMatchPoint", bundle: nil), forCellReuseIdentifier: "PassengerMatchPoint")
    }
    
    
    @IBAction func messageTapAction(_ sender: AnyObject) {
        messageLabel.isUserInteractionEnabled = false
        messageLabel.alpha = 0.5
        getThreadId()
    }
    
    func getThreadId(){
        self.view.showLoader()
        let params = [
            "receiverPersonIds": [passenger.userId]
        ]
        HttpRequest.sharedRequest.performPostRequest( Store.sharedStore.token, requestParam: params, endPoint: Endpoint.getMessageThreadId) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                self.messageLabel.isUserInteractionEnabled = true
                self.messageLabel.alpha = 1.0
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let payload = json["Payload"].array!
                var threadId = ""
                _ = payload.map { json in
                    threadId = json.string!
                }
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
                vc.messageThreadId = threadId
                vc.ride = self.upcomingRide
                //vc.passenger = self.passenger
                vc.passengerName = self.passenger.firstName
                vc.passengerId = self.passenger.id
                self.navigationController?.pushViewController(vc, animated: true)
            }
            self.view.hideLoader()
        }
    }
}

extension PassengerProfileViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if !passenger.hasDriverAcceptedBooking && !passenger.hasDriverRejectedBooking && !passenger.hasCanceledBooking{
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if passenger.phone != ""  && section == 0{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch indexPath.section{
        case 0:
            if indexPath.row == 0{
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "PassengerProfileCell") as! PassengerProfileCell
                cell.configureCell(passenger: passenger)
                return cell
            }else {
                return configurePassengerContactCell(indexPath: indexPath as NSIndexPath)
            }
        case 1:
            return configurePassengerPointCell(indexPath: indexPath as NSIndexPath)
        case 2:
            return configureAcceptRejectCell()
            
        case 3:
            return configureAcceptRejectCell()
            
        default:
            break
        }
        return UITableViewCell()
    }
    
    func configureAcceptRejectCell()->UITableViewCell{
        
        if !passenger.hasDriverAcceptedBooking && !passenger.hasDriverRejectedBooking && !passenger.hasCanceledBooking{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "AcceptRejectCell") as! AcceptRejectCell
            cell.delegate = self
            cell.configureCell()
            return cell
        }
        return UITableViewCell()
    }
    
    func configurePassengerContactCell(indexPath: NSIndexPath)->UITableViewCell{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "PassengerContactCell") as! PassengerContactCell
        cell.contactLabel.text = passenger.phone
        cell.callButton.addTarget(self, action:#selector(self.callbuttonClickedAction(sender:)), for: .touchUpInside)
        return cell
    }
    
    func callbuttonClickedAction(sender: UIButton) {
        let cell = sender.superview!.superview as? PassengerContactCell
        let numStr = cell!.contactLabel.text!.getActualContactNumber()
        Utilities.makeCall(number: numStr)
    }
    
    func configurePassengerPointCell(indexPath: NSIndexPath)->UITableViewCell{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "PassengerMatchPoint") as! PassengerMatchPoint
        var imgPath = "\(Endpoint.getLocationImage)\(passenger.start!.id)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            cell.startLocationImgView.image = image
        }
        imgPath = "\(Endpoint.getLocationImage)\(passenger.destination!.id)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            cell.destinationLocationImgView.image = image
        }//descriptionInfo
        cell.startLocationLabel.text = passenger.start!.descriptionInfo
      
        cell.destinationNameLabel.text = passenger.destination!.descriptionInfo
        cell.dateLabel.text = passenger.driverTimeAtStop
        let startLocationTap = UITapGestureRecognizer(target: self, action: #selector(self.handlePassengerImgViewTap(sender:)))
        cell.startLocationImgView.isUserInteractionEnabled = true
        cell.startLocationImgView.addGestureRecognizer(startLocationTap)
        cell.startLocationImgView.tag = indexPath.row
        let destinationLocationTap = UITapGestureRecognizer(target: self, action: #selector(self.handleLocationImgViewTap(sender:)))
        cell.destinationLocationImgView.isUserInteractionEnabled = true
        cell.destinationLocationImgView.addGestureRecognizer(destinationLocationTap)
        cell.destinationLocationImgView.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0{
                return 210  // Passenger Profile
            }else{
                return 100 // Contact Cell
            }
        case 1:
            return UITableViewAutomaticDimension
            //return 135 // PassengerMatchPoint cell
        case 2:
            return 60 // CancelReject cell
        case 3:
            return 60
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = hexStringToUIColor(hex: "EBEBF1")
        let lineView = UIView()
        lineView.backgroundColor = UIColor.lightGray
        lineView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 1)
        view.addSubview(lineView)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0.01
        case 1:
            if passenger.phone == "" {
                return 0.01
            }
            return 16
        case 2:
            return 16
        default:
            break
        }
        return 0
    }
    
    func navigateToViewController(vc: UIViewController) {
        (self.menuContainerViewController.centerViewController as AnyObject).pushViewController(vc, animated: true)
        Utilities.toggelMenuToLeft()
    }
    
    func handleLocationImgViewTap(sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPointViewController") as! MatchPointViewController
        vc.passenger = passenger
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handlePassengerImgViewTap(sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchPointViewController") as! MatchPointViewController
        vc.passenger = passenger
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PassengerProfileViewController: AcceptRejectPassengerDelegate{
    func acceptRejectPassenger(value: Int) {
        appDelegate.driverAcceptRejectPassenger = true
        if value == 0{
            acceptRejectPassenger(endPoint: Endpoint.rejectPassengerRequest,value: value)
        } else if value == 1{
            acceptRejectPassenger(endPoint: Endpoint.confirmPassengerRequest,value: value)
        }
    }
    
    func acceptRejectPassenger(endPoint: String,value: Int){
        self.view.showLoader()
        let params = [
            "passBookId": passenger.passBookId
        ]
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: endPoint) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                if value == 0{
                    self.passenger.hasDriverAcceptedBooking = false
                    self.passenger.hasDriverRejectedBooking = true
                    self.passenger.hasCanceledBooking = false
                    _ = self.navigationController?.popViewController(animated: true)
                }else{
                    self.passenger.hasDriverAcceptedBooking = true
                    self.passenger.hasDriverRejectedBooking = false
                    self.passenger.hasCanceledBooking = false
                    self.tableView.reloadData()
                }
            }
            self.view.hideLoader()
        }
    }
}

