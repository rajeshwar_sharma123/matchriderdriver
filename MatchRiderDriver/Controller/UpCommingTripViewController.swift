//
//  UpCommingTripViewController.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class UpCommingTripViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    var upcomingRides: [Ride] = []
    let noDataView = NoDataView.initView()
    var commonModel: [Any] = []
    var addedDate: [String:Bool] = [:]
    var networkViewObj: NetworkView!
    var impersonateField: UITextField!
    @IBOutlet var addView: UIView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(UpCommingTripViewController.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Kommende Fahrten"
        tableView.addSubview(refreshControl)
        checkNetworkAvailabilty()
        addView.setCircleView()
        self.menuContainerViewController.panMode = MFSideMenuPanModeDefault
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailViewController.rideSpecificNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        
        let user = Utilities.getSignInUser()
        if let user = user{
            if  user.isAdmin {
        NotificationCenter.default.addObserver(self, selector: #selector(self.impersonateNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "ImpersonateNotification"), object: nil)
            }
        }
        let bottomMenuBar = self.view.showBottomMenuBar()
        bottomMenuBar.setSpecificTabBar(item: 0)
        if appDelegate.driverAcceptRejectPassenger ||  DataContainer.sharedInstance.rideSpecificId != nil{
            appDelegate.driverAcceptRejectPassenger = false
            checkNetworkAvailabilty()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
    }
    func impersonateNotificationReceiver(_ notification: NSNotification){
        setImpersonate()
        
    }
    func setImpersonate(){
        
        let controller = ActionControllerImpersonate {
            
            if self.impersonateField.text!.isEmpty{
                return
            }
            self.view.showLoader()
            self.getImpersonateUserDetail(value: self.impersonateField.text!)
        }
        controller.addTextField { textField in
            self.impersonateField = textField
            self.impersonateField.placeholder = "Enter person Id or email"
        }
        present(controller, animated: true, completion: nil)
        
    }

    func rideSpecificNotificationReceiver(_ notification: NSNotification){
       
        checkNetworkAvailabilty()
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        DataContainer.sharedInstance.commonModel.removeAll()
        checkNetworkAvailabilty()
    }
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            if  DataContainer.sharedInstance.rideSpecificId != nil {
                DataContainer.sharedInstance.commonModel.removeAll()
                DataContainer.sharedInstance.rideSpecificId = nil
            }
            if DataContainer.sharedInstance.commonModel.count != 0{
              commonModel = DataContainer.sharedInstance.commonModel
                tableView.reloadData()
            }else{
                getUpComingActiveRides()
            }
        }
        networkViewObj?.delegate = self
    }
    
    @IBAction func addViewTapGestureAction(_ sender: Any) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "RoutesViewController") as! RoutesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    // MARK:- Service call
    
    func getImpersonateUserDetail(value: String!){
        
        var key = ""
        if value.isNumber{
            key = "Id"
        }else{
            key = "Email"
        }
        
        let params: [String:String] = [
            key: value
        ]
        
        self.view.showLoader()
        print(params)
        HttpRequest.sharedRequest.performPostRequest(Store.sharedStore.token, requestParam: params, endPoint: Endpoint.getUserTokenForAdmin) { payload in
            guard let data = payload else{
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            let json = HttpRequest.sharedRequest.parseData(data: data)
            let status = json["Status"].string
            
            if status == HttpRequest.Status.Success{
                let token = Token(json: (json["Payload"].array?.first)!)
                Store.sharedStore.token = token
                getAccountDetails()
                self.checkNetworkAvailabilty()
                DataContainer.sharedInstance.commonModel.removeAll()
                
            }else{
                let payload = json["Payload"].array!
                let dicTmp = payload.first?.dictionary!
                let error = dicTmp?["error"]?.string!
                self.showErrorMessage(message: error! )
            }
        }
    }
    
    func getUpComingActiveRides(){
        self.view.showLoader()
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token,endPoint:Endpoint.getDriverActiveRequests) { (data) in
            self.refreshControl.endRefreshing()
            guard let data = data else{
                self.view.hideLoader()
                return
            }
            
            let json = HttpRequest.sharedRequest.parseData(data: data)
            if json["Status"].string == HttpRequest.Status.Success{
                let dataArray = json["Payload"].array!
                let ride = dataArray.map { json in
                    return Ride(json: json)
                }
                self.upcomingRides = ride
                self.commonModel.removeAll()
                self.addedDate.removeAll()
                for rideTmp in ride{
                    var dic = [String:Any]()
                    let dateStr =  getDayWithDateFromDate(date: rideTmp.startTime!)
                    let addedDate = self.addedDate[dateStr]
                    if addedDate != nil{
                        dic[ShowLine] = false
                        dic["ride"] = rideTmp
                        self.commonModel.append(dic)
                    }else{
                        if dateStr.contains(Monday){
                            dic[ShowLine] = true
                            self.commonModel.append(dic)
                            dic = [String:Any]()
                            dic[ShowLine] = false
                            dic["ride"] = rideTmp
                            self.commonModel.append(dic)
                            self.addedDate[dateStr] = true
                        }else{
                            dic[ShowLine] = false
                            dic["ride"] = rideTmp
                            self.commonModel.append(dic)
                        }
                    }
                }
                if self.upcomingRides.count == 0{
                    self.tableView.backgroundView = self.noDataView
                } else{
                    self.noDataView.removeFromSuperview()
                }
                DataContainer.sharedInstance.commonModel = self.commonModel
                self.tableView.reloadData()
            }
            self.view.hideLoader()
        }
    }
}


extension UpCommingTripViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return commonModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dic = commonModel[indexPath.row]
        let tmpDic = dic as! [String:Any]
        let showLine = tmpDic[ShowLine] as! Bool
        if showLine{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "EmptyRow")! as UITableViewCell
            return cell
        }else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "UpComingRideCell") as! UpComingRideCell
            let upcomingRide = tmpDic["ride"] as! Ride
            cell.dateLabel.text = getCompleteDateFromDate(date: upcomingRide.startTime!)
            cell.messageLabel.text = "Nach: \(upcomingRide.destination!.city)"
            cell.sizeLabel.text = "Buchungen: \(upcomingRide.bookedSeats)"
            if upcomingRide.bookedSeats > 0{
                cell.imgView.image = UIImage(named: "rideshare_dark_blue")
                cell.sizeLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
            }else{
                cell.imgView.image = UIImage(named: "rideshare_grey")
                cell.sizeLabel.font = UIFont.systemFont(ofSize: 16.0)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dic = commonModel[indexPath.row]
        let tmpDic = dic as! [String:Any]
        let showLine = tmpDic[ShowLine] as! Bool
        if showLine{
            return 24
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = commonModel[indexPath.row]
        let tmpDic = dic as! [String:Any]
        let showLine = tmpDic[ShowLine] as! Bool
        if !showLine{
        let upcomingRide = tmpDic["ride"] as! Ride
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RideDetailViewController") as! RideDetailViewController
        vc.upcomingRide = upcomingRide
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UpCommingTripViewController: RideDetailViewControllerDelegate{
    func cancelTripStatus(success: Bool) {
        if success{
            DataContainer.sharedInstance.commonModel.removeAll()
            checkNetworkAvailabilty()
        }
    }
}
extension UpCommingTripViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        DataContainer.sharedInstance.commonModel.removeAll()
        checkNetworkAvailabilty()
    }
}

