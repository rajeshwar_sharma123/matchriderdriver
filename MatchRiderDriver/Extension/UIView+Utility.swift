//
//  UIView+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import RappleProgressHUD
extension UIView{
    func setBackgroundColorTransparent(){
        self.backgroundColor = UIColor(white: 0, alpha: 0.5)
    }
    func showBottomMenuBar()-> BottomMenuBar{
        let bottomMenu = BottomMenuBar.sharedInstance
        bottomMenu.frame = CGRect(x: 0, y: self.frame.height - 49, width: self.frame.width, height: 49)
        self.addSubview(bottomMenu)
       // self.bringSubview(toFront: bottomMenu)
        return bottomMenu
    }
    
    func sendShowLoginViewNotification(){
        let dict:[String: String] = ["message": "LoginView"]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowCustomView"), object: nil, userInfo: dict)
    }
    
    func sendShowRegisterViewNotification(){
        let dict:[String: String] = ["message": "RegisterView"]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowCustomView"), object: nil, userInfo: dict)
    }
    func showLoader()
    {
        RappleActivityIndicatorView.startAnimatingWithLabel("Loading...", attributes: RappleAppleAttributes)
        
    }
    func showLoaderWithMessage(message:String)
    {
        RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: RappleAppleAttributes)
    }
    func hideLoader()
    {
        RappleActivityIndicatorView.stopAnimating()
    }
    func addGradientColor(){
        let gradientLayer = CAGradientLayer()
        self.layoutIfNeeded()
        gradientLayer.frame = self.bounds
        let color1 = hexStringToUIColor(hex: MatchRiderColor).cgColor
        let color2 = hexStringToUIColor(hex: MatchRiderColor).cgColor
        let color3 = hexStringToUIColor(hex: "659966").cgColor
        gradientLayer.colors = [color1, color2, color3]
        gradientLayer.locations = [0.0, 0.5, 1]
        self.layer.addSublayer(gradientLayer)
    }
    func addGradientColorWithBaseView(){
        let gradientLayer = CAGradientLayer()
        self.layoutIfNeeded()
        gradientLayer.frame = self.bounds
        let color1 = hexStringToUIColor(hex: MatchRiderColor).cgColor
        let color2 = hexStringToUIColor(hex: MatchRiderColor).cgColor
        let color3 = hexStringToUIColor(hex: "659966").cgColor
        gradientLayer.colors = [color1, color2, color3]
        gradientLayer.locations = [0.0, 0.5, 1]
        let baseView = UIView()
        baseView.frame = self.bounds
        baseView.layer.addSublayer(gradientLayer)
        self.addSubview(baseView)
        self.sendSubview(toBack: baseView)
    }

    func setCircleView(){
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
    func setBorderWidth(){
        self.layer.borderWidth = 1
        self.layer.borderColor = hexStringToUIColor(hex: "659966").cgColor
    }
    func drowShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width:0, height: 1)
        self.layer.shadowRadius = 1
        self.layoutIfNeeded()
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
    
}
