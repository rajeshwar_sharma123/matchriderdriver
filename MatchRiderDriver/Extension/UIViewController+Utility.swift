//
//  UIViewController+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import JKNotificationPanel
extension UIViewController{
    func showSuccessMessage(message :String)
    {
        JKNotificationPanel().showNotify(withStatus: .success, belowNavigation: self.navigationController!, message: message)
    }
    func showSuccessMessageWithTitle(title: JKStatus, message: String)
    {
        JKNotificationPanel().showNotify(withStatus: title, belowNavigation: self.navigationController!, message: message)
    }
    func showErrorMessage(message :String)
    {
        let panel = JKNotificationPanel()
       // panel.showNotify(withStatus: .failed, belowNavigation: self.navigationController!)
      //  let view = panel.createDefaultView(.failed, message: message)
        let view = panel.createDefaultView(withStatus: .failed, title: message)
        view.setColor(UIColor.red)
        panel.showNotify(withView: view, belowNavigation: self.navigationController!)
    }
    func networkView(view:UIView)-> NetworkView!{
        let networkView: NetworkView!
        networkView = NetworkView.initNetworkView()
        networkView?.frame = view.bounds
        if !NetworkReachability.isConnectedToNetwork(){
            view.addSubview(networkView)
            networkView.isNetworkAvailable = false
        }
        else{
            networkView.isNetworkAvailable = true
        }
        return networkView
    }
    
  }



