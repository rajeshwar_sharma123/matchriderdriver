//
//  Protocol.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 01/03/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
protocol AcceptRejectPassengerDelegate {
    func acceptRejectPassenger(value: Int)
}
