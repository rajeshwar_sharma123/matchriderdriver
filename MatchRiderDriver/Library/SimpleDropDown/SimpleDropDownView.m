//
//  DropDownView.m
//  KrishnaDropDown
//
//  Created by daffolapmac on 17/05/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

#import "SimpleDropDownView.h"
#import "SimpleDropDownCell.h"
@implementation SimpleDropDownView
{
    UIView *baseView;
}

- (id)init{
    SimpleDropDownView *dropDownView = [[[NSBundle mainBundle] loadNibNamed:@"SimpleDropDownView" owner:nil options:nil] firstObject];
    return dropDownView;
 }

-(void)setDropDownOnView:(UIView *)onView belowView:(UIView *)belowView andData:(NSArray *)dataArry{
    self.dataArry  = [NSArray array];
    [self.tableView reloadData];
    self.dataArry = dataArry;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height - 44;
    
    CGFloat viewHeight = 50*dataArry.count;
    if (dataArry.count > 6){
        viewHeight = 50*6;
    }
   
    
    CGRect frameOnView = [belowView convertRect:belowView.bounds toView:onView];
    CGFloat topMargin = frameOnView.origin.y + frameOnView.size.height;
    CGFloat difference = screenHeight - topMargin;
    if(difference < viewHeight){
        self.frame = CGRectMake(frameOnView.origin.x, frameOnView.origin.y - viewHeight + 1, frameOnView.size.width, viewHeight);
    }else{
        self.frame = CGRectMake(frameOnView.origin.x, frameOnView.origin.y + frameOnView.size.height, frameOnView.size.width, viewHeight);
    }
    [self removeFromSuperview];
    
    baseView = [[UIView alloc]init];
    baseView.frame = screenRect;
    baseView.backgroundColor = [UIColor clearColor];
    [onView addSubview:baseView];
    UITapGestureRecognizer *baseViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(baseViewTapAction:)];
    [baseView addGestureRecognizer:baseViewTap];
    
    _onView = onView;
    _belowView = belowView;
    [onView addSubview:self];
    [self.tableView reloadData];
}
- (void)baseViewTapAction:(UITapGestureRecognizer*)sender {
    [self toggle];
    
}

-(void)awakeFromNib{
    _isVisible = false;
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(-2, 2);
    self.layer.shadowRadius = 10;
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SimpleDropDownCell" bundle:nil] forCellReuseIdentifier:@"SimpleDropDownCell"];
    [super awakeFromNib];
    
}

- (void)toggle{
    if (_isVisible){
        _isVisible = false;
        [self setHidden:true];
        [baseView removeFromSuperview];
    }
    else{
        _isVisible = true;
        [self setHidden:false];
    }
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArry.count;
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SimpleDropDownCell";
    
    SimpleDropDownCell *cell = (SimpleDropDownCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SimpleDropDownCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *name =  [_dataArry objectAtIndex:indexPath.row];
    cell.labelName.text = name;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self toggle];
    NSString *name =  [_dataArry objectAtIndex:indexPath.row];
    [_delegate selectedName:name andContainerView:_belowView];
    [self removeFromSuperview];
}
@end
