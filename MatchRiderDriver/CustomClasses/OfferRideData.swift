//
//  OfferRideData.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 07/09/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//


import UIKit

class OfferRideData: NSObject {
    
    var routeAStartPreconfiguredRouteLocationId: Int!
    var routeADestPreconfiguredRouteLocationId: Int!
    var routeBStartPreconfiguredRouteLocationId: Int!
    var routeBDestPreconfiguredRouteLocationId: Int!
    static let sharedInstance = OfferRideData()
}
