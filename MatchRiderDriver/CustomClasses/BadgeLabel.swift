//
//  BadgeLabel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 28/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class BadgeLabel: UILabel {
   
    override func layoutSubviews() {
        self.backgroundColor = hexStringToUIColor(hex: "E88889")
        self.layer.masksToBounds = true
        self.textAlignment = .center
        self.textColor = UIColor.white
        self.font = UIFont.systemFont(ofSize: 11.0)
        self.layer.cornerRadius = self.frame.width/2
    }
    
}
