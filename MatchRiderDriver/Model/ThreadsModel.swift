//
//  ThreadsModel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 07/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
import SwiftyJSON
class ThreadsModel {
    
    let messagePostId: Int
    let messageThreadId: String
    let personUserId: String
    let messageThreadParticipantId: String
    let driverRequestId: Int
    let messagePosting : String
    let dateCreated: String
    let personId: Int
    let personName: String
    let personPhoto: String
    let isUnread : Bool
    
    init(json: JSON) {
        
        personName = getStringValue(json: json["PersonName"])
        messagePostId = getIntValue(json:json["MessagePostId"])
        messageThreadId = getStringValue(json: json["MessageThreadId"])
        messageThreadParticipantId = getStringValue(json: json["MessageThreadParticipantId"])
        driverRequestId = getIntValue(json: json["DriverRequestId"])
        messagePosting = getStringValue(json: json["MessagePosting"])
        dateCreated = getStringValue(json: json["DateCreated"])
        personId = getIntValue(json:json["PersonId"])
        personPhoto = getStringValue(json: json["PersonPhoto"])
        isUnread = json["IsUnread"].bool!
        personUserId = getStringValue(json: json["PersonUserId"])
    }
    
}
