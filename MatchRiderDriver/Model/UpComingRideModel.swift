//
//  UpComingRideModel.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 27/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON
class UpComingRideModel {
    let destLocationLongitude: Double
    let startLocationDescription: String
    let numberPassCancelledPassengers: Int
    let startTimeString: String
    let driverStatus: Int
    let passengerRequestId: Int
    let rideCost: Double
    let startLocationCountry: String
    let startLocationLongitude: Double
    let startTimeUtc: String
    let driverCanceled: Bool
    let rideDistanceKm: Int
    
    let polyline: MKPolyline
    
    let vehicleDescription: String
    let destLocationId: Int
    let destLocationMatchPointName: String
    let destLocationDependentCity: String
    let freeSeats: Int
    let isUserDriver: Bool
    let numberRejectedPassengers: Int
    let startTime: String
    let endTime: String
    
    let tzInfo: String
    
    let startLocationId: Int
    let startLocationMatchPointName: String
    let startLocationDependentCity: String
    let startLocationState: String
    let driverStatusText: String
    let startLocationCity: String
    
    let passengers: [PassengerListModel]?
    
    let destLocationDescription: String
    let driverCanceledTime: String
    let currentTime: String
    let startLocationName: String
    let rideStatus: Int
    let destLocationLatitude: Double
    let numberUnconfirmedPassengers: Int
    let vehicleId: Int
    let vehicleImageId: Int
    let driverRequestId: Int
    let destLocationName: String
    let destLocationAddress: String
    let rideDurationSec: Int
    let isRegularlyScheduled: Bool
    let commuteId: Int
    let startLocationAddress: String
    let destLocationCity: String
    let bookedDriver: BookedDriver?
    let driverUserId: String
    let numberConfirmedPassengers: Int
    let startLocationLatitude: Double
    
    init(json: JSON) {
        self.destLocationLongitude = getDoubleValue(json:json["DestLocationLongitude"])
        self.startLocationDescription = getStringValue(json:json["StartLocationDescription"])
        self.numberPassCancelledPassengers = getIntValue(json: json["NumberPassCancelledPassengers"]) 
        self.startTimeString = getStringValue(json:json["StartTimeString"])
        self.driverStatus = getIntValue(json:json["DriverStatus"])
        self.passengerRequestId = getIntValue(json:json["PassengerRequestId"])
        self.rideCost = getDoubleValue(json: json["RideCost"])
        self.startLocationCountry = getStringValue(json:json["StartLocationCountry"])
        self.startLocationLongitude = getDoubleValue(json:json["StartLocationLongitude"])
        self.startTimeUtc = getStringValue(json:json["StartTimeUtc"])
        self.driverCanceled = json["DriverCanceled"].bool!
        self.rideDistanceKm = getIntValue(json:json["RideDistanceKm"])
              
        let rideRoute = json["rideRoute"].array!
        var coordinates = rideRoute.map { json -> CLLocationCoordinate2D in
            let latitude = json["latitude"].double!
            let longitude = json["longitude"].double!
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        self.polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
        
        self.vehicleDescription = getStringValue(json: json["VehicleDescription"])
        self.destLocationId = getIntValue(json:json["DestLocationId"])
        self.destLocationMatchPointName = getStringValue(json:json["DestLocationMatchPointName"])
        self.destLocationDependentCity = getStringValue(json: json["DestLocationDependentCity"])
        self.freeSeats = getIntValue(json:json["FreeSeats"])
        self.isUserDriver = json["IsUserDriver"].bool!
        self.numberRejectedPassengers = getIntValue(json:json["NumberRejectedPassengers"])
        self.startTime = getStringValue(json:json["StartTime"])
        self.endTime = getStringValue(json:json["EndTime"])
        
        
        
        self.tzInfo = ""
        
        self.startLocationId = getIntValue(json:json["StartLocationId"])
        self.startLocationMatchPointName = getStringValue(json:json["StartLocationMatchPointName"])
       
        
        self.startLocationDependentCity = getStringValue(json: json["StartLocationDependentCity"])
        self.startLocationState = getStringValue(json:json["StartLocationState"])
        self.driverStatusText = getStringValue(json:json["DriverStatusText"])
        self.startLocationCity = getStringValue(json:json["StartLocationCity"])
        let passengerArr = json["Passengers"].array!
       
        let passenger = passengerArr.map { json in
            return PassengerListModel(json: json)
        }
        
        self.passengers = passenger
        self.destLocationDescription = getStringValue(json:json["DestLocationDescription"])
        self.driverCanceledTime = getStringValue(json:json["DriverCanceledTime"])
        self.currentTime = getStringValue(json:json["CurrentTime"])
        self.startLocationName = getStringValue(json:json["StartLocationName"])
        self.rideStatus = getIntValue(json:json["RideStatus"])
        self.destLocationLatitude = getDoubleValue(json:json["DestLocationLatitude"])
        self.numberUnconfirmedPassengers = getIntValue(json:json["NumberUnconfirmedPassengers"])
        self.vehicleId = getIntValue(json:json["VehicleId"])
        self.vehicleImageId = getIntValue(json:json["VehicleImageId"])
        self.driverRequestId = getIntValue(json:json["DriverRequestId"])
        self.destLocationName = getStringValue(json:json["DestLocationName"])
        self.destLocationAddress = getStringValue(json:json["DestLocationAddress"])
        self.rideDurationSec = getIntValue(json:json["RideDurationSec"])
        self.isRegularlyScheduled = json["IsRegularlyScheduled"].bool!
        self.commuteId = getIntValue(json:json["CommuteId"])
        self.startLocationAddress = getStringValue(json:json["StartLocationAddress"])
        self.destLocationCity = getStringValue(json:json["DestLocationCity"])
        
        self.bookedDriver = json["BookedDriver"] != JSON.null ? BookedDriver(json: json["BookedDriver"]) : nil
        
        self.driverUserId = getStringValue(json:json["DriverUserId"])
        self.numberConfirmedPassengers = getIntValue(json:json["NumberConfirmedPassengers"])
        self.startLocationLatitude = getDoubleValue(json:json["StartLocationLatitude"])
             
    }
    
}

struct BookedDriver {
    let passengerRequestId: Int
    let passBookId: Int
    let userId: String
    let firstName: String
    init(json: JSON) {
        self.passengerRequestId = getIntValue(json:json["PassengerRequestId"])
        self.passBookId = getIntValue(json:json["PassBookId"])
        self.userId = getStringValue(json:json["UserId"])
        self.firstName = getStringValue(json:json["FirstName"])
    }
}
