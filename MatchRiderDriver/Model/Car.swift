import SwiftyJSON
import Foundation
struct Car {
  var id: Int
  var photo: String
  var photoCar: UIImage?
  var model: String
  var color: String
  var license: String
  var make: String
  var seats: Int
  var colorRGBA: String
  
  init(json: JSON) {
    id = getIntValue(json:json["id"])
    photo = getStringValue(json: json["photo"])
    model = getStringValue(json:json["model"])
    color = getStringValue(json:json["color"])
    license = getStringValue(json:json["license"])
    make = getStringValue(json:json["make"])
    colorRGBA = getStringValue(json:json["colorRGBA"])
    seats = getIntValue(json:json["seats"])
    photoCar = nil
  }
    
}


