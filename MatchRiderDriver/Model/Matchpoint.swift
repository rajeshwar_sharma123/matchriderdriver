
import SwiftyJSON
import Foundation

import UIKit
import MapKit

class Matchpoint: NSObject, MKAnnotation {
  
  let id: Int
  let latitude: Double
  let longitude: Double
  let name: String
  let whereToStand: String
  let descriptionInfo: String
  let street: String
  let postalCode: String
  let city: String
  let dependentCity: String
  let state: String
  let birdFlyDistanceFromStart: Int
  let photo: String
  let delay: Int
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  var title: String? {
    return descriptionInfo
  }
  
  init(json: JSON) {
    id = getIntValue(json:json["id"])
    latitude = getDoubleValue(json:json["latitude"])
    longitude = getDoubleValue(json:json["longitude"])
    name = getStringValue(json:json["name"])
    whereToStand = getStringValue(json:json["whereToStand"])
    descriptionInfo = getStringValue(json:json["description"])
    street = getStringValue(json:json["street"])
    postalCode = getStringValue(json:json["postalCode"])
    city = getStringValue(json:json["city"])
    dependentCity = getStringValue(json:json["dependentCity"])
    state = getStringValue(json:json["state"])
    photo = getStringValue(json:json["photo"])
    delay = getIntValue(json:json["delay"]) / 60
    birdFlyDistanceFromStart = getIntValue(json:json["birdFlyDistanceFromStart"])
  }
     
}


class DriverLocation: NSObject, MKAnnotation{
  
  let latitude: Double
  let longitude: Double
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  var title: String? {
    return "Fahrer"
  }
  
  init(lat: Double, long: Double) {
    latitude = lat
    longitude = long
  }
  
}
