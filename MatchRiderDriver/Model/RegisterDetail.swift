//
//  RegisterDetail.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 09/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import UIKit

class RegisterDetail: NSObject {
    
    private override init() { }
    
    static let sharedInstance = RegisterDetail()
    
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    //var gender = true
    var termsCondition = false
    
    
    func setInitialValue(){
         firstName = ""
         lastName = ""
         email = ""
         password = ""
         confirmPassword = ""
         //ender = true
         termsCondition = false

    }
    
}
