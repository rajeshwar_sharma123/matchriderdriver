//
//  PassengerListModel.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 01/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import SwiftyJSON
class PassengerListModel {
    let id: Int
    let firstName: String
    let lastName: String
    let email: String
    let phone: String
    let passBookId: Int
    let gender: String
    let photo: String
    let score: Int
    let description: String
    
    let userId: String
    var hasCanceledBooking: Bool
    var hasDriverAcceptedBooking: Bool
    var hasDriverRejectedBooking: Bool
    
    let isMobilePhoneValidated: Bool
    let driverTimeAtStop: String
    let driverTimeAtStopDate: Date?
    let driverTimeAtDropDate: Date?
    
    let start: Matchpoint?
    let destination: Matchpoint?
        
    
    init(json: JSON) {
        self.id = getIntValue(json:json["id"])
        self.firstName = getStringValue(json:json["firstName"])
        self.lastName = getStringValue(json:json["lastName"])
        self.email = getStringValue(json:json["email"])
        self.phone = getStringValue(json:json["phone"])
        self.passBookId = getIntValue(json:json["passBookId"])
        self.gender = getStringValue(json:json["gender"])
        self.photo = getStringValue(json:json["photo"])
        self.score = getIntValue(json:json["score"])
        self.description = getStringValue(json:json["description"])
        
        self.userId = getStringValue(json:json["userId"])
        self.hasCanceledBooking = json["hasCanceledBooking"].bool!
        self.hasDriverAcceptedBooking = json["hasDriverAcceptedBooking"].bool!
        self.hasDriverRejectedBooking = json["hasDriverRejectedBooking"].bool!
        
        self.isMobilePhoneValidated = json["isMobilePhoneValidated"].bool!
        self.driverTimeAtStop = json["driverTimeAtStop"].string != nil ? getDriverTimeAtStop(json["driverTimeAtStop"].string!) : ""
        
        start = json["start"] != JSON.null ? Matchpoint(json: json["start"]) : nil
        driverTimeAtStopDate = json["driverTimeAtStop"].string != nil ? convertToDate(json["driverTimeAtStop"].string!) : nil
        
        driverTimeAtDropDate = json["driverTimeAtDrop"].string != nil ? convertToDate(json["driverTimeAtDrop"].string!) : nil
        
        destination = json["destination"] != JSON.null ? Matchpoint(json: json["destination"]) : nil
     }
  
}

