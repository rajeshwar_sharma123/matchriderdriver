//
//  SuperHighway.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 18/08/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
//import MapKit
import SwiftyJSON
struct SuperHighway {
    
    let destCity: String
    let isSuperHighway: Bool
    let rideDateTime: Date
    let startLocationId: Int
    
    let preconfiguredRouteCustomizationId: Int
    let startHighwayDescription: String
    let preconfiguredRouteId: Int
    let startCity: String
    
    let highwayDescription: String
    let personId: Int
    let startLocationName: String
    let preconfiguredHighwayId: Int
    
    let startPreconfiguredRouteId: Int
    let destLocationName: String
    let destPreconfiguredRouteId: Int
    let dynamicRideGeneration: Bool
    
    let destHighwayDescription: String
    let destLocationId: Int

    
    let routeAStartPreconfiguredRouteCustomizationId: Int
    let routeAStartPreconfiguredRouteLocationId: Int
    let routeAStartPreconfiguredRouteLocationName: String
    let routeAStartPreconfiguredRouteLocationDesc: String
    
    let routeADestPreconfiguredRouteLocationId: Int
    let routeADestPreconfiguredRouteLocationName: String
    let routeADestPreconfiguredRouteLocationDesc: String
    let routeBStartPreconfiguredRouteCustomizationId: Int
    
    let routeBStartPreconfiguredRouteLocationId: Int
    let routeBStartPreconfiguredRouteLocationName: String
    let routeBStartPreconfiguredRouteLocationDesc: String
    let routeBDestPreconfiguredRouteLocationId: Int
    
    let routeBDestPreconfiguredRouteLocationName: String
    let routeBDestPreconfiguredRouteLocationDesc: String
    let pointsEarned: Int
    let routeAMaxPoints: Int
    
    let routeBMaxPoints: Int
    var seats: Int
    
   
    
    init(json: JSON) {
        
        self.destCity = getStringValue(json: json["destCity"])
        self.isSuperHighway = json["isSuperHighway"].bool!
        self.rideDateTime = getDateFromTimeString(timeStr: json["rideDateTime"].string!)
        self.startLocationId = getIntValue(json:json["startLocationId"])
       
        self.preconfiguredRouteCustomizationId = getIntValue(json:json["preconfiguredRouteCustomizationId"])
        self.startHighwayDescription = getStringValue(json: json["startHighwayDescription"])
        self.preconfiguredRouteId = getIntValue(json:json["preconfiguredRouteId"])
        self.startCity = getStringValue(json: json["startCity"])
        
        
        self.highwayDescription = getStringValue(json: json["highwayDescription"])
        self.personId = getIntValue(json:json["personId"])
        self.startLocationName = getStringValue(json: json["startLocationName"])
        self.preconfiguredHighwayId = getIntValue(json:json["preconfiguredHighwayId"])
        
        
        self.startPreconfiguredRouteId = getIntValue(json:json["startPreconfiguredRouteId"])
        self.destLocationName = getStringValue(json: json["destLocationName"])
        self.destPreconfiguredRouteId = getIntValue(json:json["destPreconfiguredRouteId"])
        self.dynamicRideGeneration = json["dynamicRideGeneration"].bool!
        
        
        self.destLocationId = getIntValue(json:json["destLocationId"])
        self.destHighwayDescription = getStringValue(json: json["destHighwayDescription"])
        
        self.routeAStartPreconfiguredRouteCustomizationId = getIntValue(json:json["routeAStartPreconfiguredRouteCustomizationId"])
        self.routeAStartPreconfiguredRouteLocationId = getIntValue(json:json["routeAStartPreconfiguredRouteLocationId"])
        self.routeAStartPreconfiguredRouteLocationName = getStringValue(json: json["routeAStartPreconfiguredRouteLocationName"])
        self.routeAStartPreconfiguredRouteLocationDesc = getStringValue(json: json["routeAStartPreconfiguredRouteLocationDesc"])
        
        
        self.routeADestPreconfiguredRouteLocationId = getIntValue(json:json["routeADestPreconfiguredRouteLocationId"])
        self.routeADestPreconfiguredRouteLocationName = getStringValue(json: json["routeADestPreconfiguredRouteLocationName"])
        self.routeADestPreconfiguredRouteLocationDesc = getStringValue(json: json["routeADestPreconfiguredRouteLocationDesc"])
        self.routeBStartPreconfiguredRouteCustomizationId = getIntValue(json:json["routeBStartPreconfiguredRouteCustomizationId"])
        
        self.routeBStartPreconfiguredRouteLocationId = getIntValue(json:json["routeBStartPreconfiguredRouteLocationId"])
        self.routeBStartPreconfiguredRouteLocationName = getStringValue(json: json["routeBStartPreconfiguredRouteLocationName"])
        self.routeBStartPreconfiguredRouteLocationDesc = getStringValue(json: json["routeBStartPreconfiguredRouteLocationDesc"])
        self.routeBDestPreconfiguredRouteLocationId = getIntValue(json:json["routeBDestPreconfiguredRouteLocationId"])
        
      
        self.routeBDestPreconfiguredRouteLocationName = getStringValue(json: json["routeBDestPreconfiguredRouteLocationName"])
        self.routeBDestPreconfiguredRouteLocationDesc = getStringValue(json: json["routeBDestPreconfiguredRouteLocationDesc"])
        
        
        self.pointsEarned = getIntValue(json:json["pointsEarned"])
        self.routeAMaxPoints = getIntValue(json:json["routeAMaxPoints"])
        self.routeBMaxPoints = getIntValue(json:json["routeBMaxPoints"])
        self.seats = getIntValue(json:json["seats"])
       
        
    }
}









