//
//  Holiday.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 12/04/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
import SwiftyJSON
class Holiday {
    let holidayEnd: Date?
    let holidayStart: Date?
    let personId: Int
    let id: Int
    
    init(json: JSON) {
        holidayEnd = json["HolidayEnd"].string != nil ? getDateFromTimeString(timeStr: json["HolidayEnd"].string!) : nil
        holidayStart = json["HolidayStart"].string != nil ? getDateFromTimeString(timeStr: json["HolidayStart"].string!) : nil
        personId = getIntValue(json: json["PersonId"])
        id = getIntValue(json:json["Id"])
    }

}
