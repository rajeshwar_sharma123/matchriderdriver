//
//  MatchPointView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class MatchPointView: UIView {
    @IBOutlet var matchPointImageView: UIImageView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var locationNameLabel: UILabel!
    @IBOutlet var locationDescriptionLabel: UILabel!
    @IBOutlet var fullAddressLabel: UILabel!
    @IBOutlet var whereToStandLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    var passenger: PassengerListModel!
    var matchPoint: MatchPointModel!
    override func awakeFromNib() {
        self.setBackgroundColorTransparent()
       // getMatchPointInfo()
    }
    class func initView(passenger: PassengerListModel)->MatchPointView{
        let matchPointView = Bundle.main.loadNibNamed("MatchPointView", owner: self, options: nil)?.first as! MatchPointView
        matchPointView.passenger = passenger
        return matchPointView
    }
    
    
    @IBAction func cancelButtonAction(_ sender: AnyObject) {
        self.removeFromSuperview()
    }
    func getMatchPointInfo(){
        
        self.showLoader()
        let endPoint = "\(Endpoint.getMatchPointInfo)\(passenger.start!.id)"
        HttpRequest.sharedRequest.performGetRequest(Store.sharedStore.token,endPoint:endPoint) { (data) in
            guard let data = data else{
                self.hideLoader()
                return
            }
            let jsonTmp = HttpRequest.sharedRequest.parseData(data: data)
            let matchPoint = MatchPointModel(json: jsonTmp)
            self.matchPoint = matchPoint
            self.hideLoader()
            self.setData()
            
        }
    }
    
    func setData(){
        let imgPath = "\(Endpoint.getLocationImage)\(matchPoint.locationId)"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.matchPointImageView.image = image
        }
        locationNameLabel.text = "\(matchPoint.locationName) (Karte)"
        locationDescriptionLabel.text = matchPoint.locationDescription
        fullAddressLabel.text = matchPoint.fullAddress
        whereToStandLabel.text = matchPoint.whereToStand
        timeLabel.text = "Zeit: \(passenger.driverTimeAtStop.replacingOccurrences(of: "-", with: " "))"  
        
    }

    @IBAction func matchPointImageViewTapAction(_ sender: AnyObject) {
        let vc = Utilities.getTopViewController()
        let matchPointView = MatchPointMapView.initView(matchPoint: matchPoint)
        matchPointView.showMatchPoint()
        matchPointView.frame = self.bounds
        vc.navigationController?.view.addSubview(matchPointView)
       
    }
}
