//
//  MatchPointMapView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
import MapKit
class MatchPointMapView: UIView {

    @IBOutlet var mapView: MKMapView!
    var matchPoint: MatchPointModel!
    override func awakeFromNib() {
        self.setBackgroundColorTransparent()
        mapView.delegate = self
       
    }
    class func initView(matchPoint: MatchPointModel)->MatchPointMapView{
        let matchPointMapView = Bundle.main.loadNibNamed("MatchPointMapView", owner: self, options: nil)?.first as! MatchPointMapView
        matchPointMapView.matchPoint = matchPoint
        return matchPointMapView
    }
    func showMatchPoint(){
        let annotation = Annotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: matchPoint.latitude, longitude: matchPoint.longitude)
        annotation.title = matchPoint.locationNameWithCity
        mapView.addAnnotation(annotation)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.7, longitudeDelta: 0.7))
        self.mapView.setRegion(region, animated: true)

    }
    @IBAction func cancelButtonAction(_ sender: AnyObject) {
        self.removeFromSuperview()
    }

}
extension MatchPointMapView: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let matchpoint = annotation as? Annotation else {
            return nil
        }
        let view = MKAnnotationView(annotation: matchpoint, reuseIdentifier: "Matchpoint")
        view.image = UIImage(named: "markerMP")!
        //view.layer.zPosition = 1
        
        view.centerOffset = CGPoint(x: 0, y: -18)
        view.canShowCallout = true
        return view
}
}
