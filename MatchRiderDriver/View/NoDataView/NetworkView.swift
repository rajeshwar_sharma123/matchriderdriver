//
//  NetworkView.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 28/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
protocol NetworkViewDelegate {
    func refreshButtonClicked()
}
class NetworkView: UIView {
    var delegate: NetworkViewDelegate!
    var isNetworkAvailable: Bool!
   
   class func initNetworkView()->NetworkView{
        let networkView = Bundle.main.loadNibNamed("NetworkView", owner: self, options: nil)?.first as? NetworkView
            return networkView!
    }
    @IBAction func refreshButtonAction(_ sender: AnyObject) {
        self.removeFromSuperview()
        let dispatchTime = DispatchTime.now() + .nanoseconds(1)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.delegate.refreshButtonClicked()
        }
       
    }
}
