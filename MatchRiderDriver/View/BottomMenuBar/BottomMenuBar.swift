//
//  BottomMenuBar.swift
//  MatchRider-Bussiness
//
//  Created by daffolapmac on 20/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class BottomMenuBar: UIView,UITabBarDelegate {

    @IBOutlet var tabBarItem1: UITabBarItem!
    @IBOutlet var tabBar: UITabBar!
    var lastSelectedItem: Int!
    static let sharedInstance: BottomMenuBar = BottomMenuBar.initWithDefaultValue()
    
   override func awakeFromNib() {
       lastSelectedItem = 1
   
    UITabBar.appearance().barTintColor = hexStringToUIColor(hex: MatchRiderColor)
    tabBar.isTranslucent = false
    //UITabBar.appearance().barTintColor = hexStringToUIColor(hex: "151C13")
       tabBar.selectedItem = (tabBar.items?[lastSelectedItem])! as UITabBarItem
    UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)], for:.normal)
    UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.selected)
    
    
    tabBar.items?[0].image = tabBar.items?[0].image?.withRenderingMode(.alwaysTemplate)
    tabBar.items?[1].image = tabBar.items?[1].image?.withRenderingMode(.alwaysTemplate)
    tabBar.items?[2].image = tabBar.items?[2].image?.withRenderingMode(.alwaysTemplate)
    if #available(iOS 10.0, *) {
        tabBar.unselectedItemTintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)
    } else {
        // Fallback on earlier versions
    }
    
    }
   class func initWithDefaultValue()->BottomMenuBar{
        let instance = Bundle.main.loadNibNamed("BottomMenuBar", owner: self, options: nil)?.first as! BottomMenuBar
        return instance
 
    }
    func setSpecificTabBar(item: Int){
        lastSelectedItem = item
        tabBar.selectedItem = (tabBar.items?[lastSelectedItem])! as UITabBarItem
    }
     func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item...\(item.tag)")
        if item.tag != 2 {
            lastSelectedItem = item.tag
        }

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navContr = Utilities.getTopViewController().navigationController
        switch item.tag {
        case 0:
            let vc = storyBoard.instantiateViewController(withIdentifier: "UpCommingTripViewController") as! UpCommingTripViewController
            navContr?.setViewControllers([vc], animated: false)
        case 1:
            let vc = storyBoard.instantiateViewController(withIdentifier: "GPSTrackingViewController") as! GPSTrackingViewController
            navContr?.setViewControllers([vc], animated: false)
        case 2:
            tabBar.selectedItem = (tabBar.items?[lastSelectedItem])! as UITabBarItem
            Utilities.toggelMenuToLeft()
            default:
            break
        }
        
        
        
        
    }

   }
extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
       // let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        context.clip(to: rect, mask: self.cgImage!)
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
