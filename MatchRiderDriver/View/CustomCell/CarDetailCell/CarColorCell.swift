//
//  CarColorCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class CarColorCell: UITableViewCell {

    @IBOutlet var colorView: UIView!
    @IBOutlet var chooseVehicleColor: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        colorView.setCircleView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
