//
//  ProfileViewCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 23/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class ProfileViewCell: UITableViewCell {
    @IBOutlet var topView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var updateProfileView: UIView!
    @IBOutlet var userImgView: UIImageView!
    let vc = Utilities.getTopViewController()
    var icon: UIImage?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImgView.setCircleView()
        updateProfileView.setCircleView()
        userImgView.setBorderWidth()
        userImgView.clipsToBounds = true
        
        updateProfileView.isHidden = true
    }
    override func layoutSubviews() {
        topView.addGradientColorWithBaseView()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(){
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(updateProfileViewTapAction(_:)))
        let user = Utilities.getSignInUser()
        nameLabel.text = "\(user!.firstName) \(user!.lastName)"
        emailLabel.text = user!.email
        let imgPath = "\(Endpoint.getPersonImage)\(user!.id)/l"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: false) { image in
            if image != nil{
              self.userImgView.image = image  
            }
            
        }
        
        updateProfileView.addGestureRecognizer(tap)
    }
    func updateProfileViewTapAction(_ sender: UITapGestureRecognizer) {
       
        takePicture()
    }
    func takePicture() {
        //let vc = Utilities.getTopViewController()
        let controller = UIAlertController(title: "Foto", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel) { action in
            controller.dismiss(animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Kamera", style: .default) { action in
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.cameraDevice = .front
            picker.showsCameraControls = true
            picker.allowsEditing = false
            picker.delegate = self
            self.vc.present(picker, animated: false, completion: nil)
        }
        let library = UIAlertAction(title: "Fotosammlung", style: .default) { action in
            let picker = UIImagePickerController()
            picker.navigationBar.isTranslucent = false
            picker.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white
            ]
            picker.navigationBar.barTintColor = hexStringToUIColor(hex: MatchRiderColor)
            picker.navigationBar.tintColor = UIColor.white;
            picker.sourceType = .photoLibrary
            picker.allowsEditing = false
            picker.delegate = self
            self.vc.present(picker, animated: true, completion: nil)
        }
        controller.addAction(cancel)
        controller.addAction(camera)
        controller.addAction(library)
        vc.navigationController?.present(controller, animated: true, completion: nil)
    }

}
extension ProfileViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            vc.navigationController?.dismiss(animated: true, completion: nil)
        }
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        let size = CGSize(width: 480, height: 640)
        let imgTmp = image.resizeImage(targetSize: size)
        icon = imgTmp
        self.userImgView.image = imgTmp
        
    }
    
}

extension ProfileViewCell: ProfileViewControllerDelegate{
    func updateUserImage() {
        UserDetail.sharedInstance.image = userImgView.image
        if updateProfileView.isHidden {
            updateProfileView.isHidden = false
        }
        else{
            updateProfileView.isHidden = true
        }
    }
}


