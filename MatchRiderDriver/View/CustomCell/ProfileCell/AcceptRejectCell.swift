//
//  AcceptRejectCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class AcceptRejectCell: UITableViewCell {
    @IBOutlet var rejectView: UIView!
    @IBOutlet var acceptView: UIView!
    var delegate: AcceptRejectPassengerDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rejectView.layer.cornerRadius = 3
        rejectView.layer.masksToBounds = true
        acceptView.layer.cornerRadius = 3
        acceptView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(){
        let acceptTap = UITapGestureRecognizer(target: self, action:#selector(acceptViewTapAction(_:)))
        acceptView.addGestureRecognizer(acceptTap)
        
        let rejectTap = UITapGestureRecognizer(target: self, action:#selector(rejectViewTapAction(_:)))
        rejectView.addGestureRecognizer(rejectTap)
    }
    
    func acceptViewTapAction(_ sender: UITapGestureRecognizer) {
        print("Accept")
        delegate.acceptRejectPassenger(value: 1)
    }
     func rejectViewTapAction(_ sender: UITapGestureRecognizer) {
        print("reject")
        delegate.acceptRejectPassenger(value: 0)
    }
    
}
