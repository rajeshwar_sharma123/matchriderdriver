//
//  PassengerProfileCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 24/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class PassengerProfileCell: UITableViewCell {
    @IBOutlet var topView: UIView!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var emailIdLabel: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImgView.setCircleView()
        userImgView.setBorderWidth()
        
    }
    override func layoutSubviews() {
        topView.addGradientColorWithBaseView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(passenger: PassengerListModel){
        userNameLabel.text = passenger.firstName
        emailIdLabel.text = passenger.email
        //let imgPath = "\(Endpoint.getPersonImage)\(passenger.userId)/m"
        let imgPath = "\(Endpoint.getPersonImage)\(passenger.userId)/l"
        HttpRequest.sharedRequest.image(Store.sharedStore.token,url: imgPath, useCache: true) { image in
            self.userImgView.image = image
        }

    }
}
