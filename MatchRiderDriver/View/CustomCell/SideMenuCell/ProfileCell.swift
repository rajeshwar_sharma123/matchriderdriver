//
//  ProfileCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 10/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
