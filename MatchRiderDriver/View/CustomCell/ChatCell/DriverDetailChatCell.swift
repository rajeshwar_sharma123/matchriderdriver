//
//  DriverDetailChatCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 02/03/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class DriverDetailChatCell: UITableViewCell {

    @IBOutlet var driverImageView: IconView!
    @IBOutlet var driverNameLabel: UILabel!
    //@IBOutlet var ratingView: Rating!
    @IBOutlet var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func bindDataWithCell(_ ride:Ride){
        
//        var nameStr = ""
//        if ride.passengers!.count != 0{
//         nameStr = ride.passengers!.first!.firstName
//        if ride.passengers!.count > 1{
//           nameStr = "\(ride.passengers!.first!.firstName) and \(ride.passengers!.count - 1) more"
//        }
//        }
//        driverNameLabel.text = nameStr
        var detailStr = ""
        let date = ride.startTime
        if date != nil{
            detailStr = getDateWithDay(date!,timeFlag: true)
        }
        let startMatchPoint = ride.start
        if startMatchPoint != nil{
            detailStr = "\(detailStr), von \(startMatchPoint!.descriptionInfo)"
        }
        let destinationMatchPoint = ride.destination
        if startMatchPoint != nil{
            detailStr = "\(detailStr) nach \(destinationMatchPoint!.descriptionInfo)"
        }
        driverNameLabel.text = detailStr
        let driver = ride.driver
        if driver != nil{
            
//            HttpRequest.sharedRequest.image(Store.sharedStore.token,url: driver!.photo, useCache: true) { image in
//                self.driverImageView.image = image
//            }
            
          //  ratingView.setCount(ride.driver!.score)
        }
        
        self.driverImageView.image = UIImage(named: "rideshare_dark_blue")
        
    }
    func bindCellWithBlankData(){
        driverNameLabel.text = ""
        detailLabel.text = ""
       // ratingView.setCount(5)
        self.driverImageView.image = UIImage(named: "rideshare_dark_blue")
    }

}
