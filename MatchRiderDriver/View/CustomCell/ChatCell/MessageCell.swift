//
//  MessageCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet var userImageView: IconView!
    @IBOutlet var arrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
