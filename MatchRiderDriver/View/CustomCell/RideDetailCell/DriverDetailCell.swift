//
//  DriverDetailCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 01/02/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class DriverDetailCell: UITableViewCell {

    @IBOutlet var driverImageView: UIImageView!
    @IBOutlet var carImageView: UIImageView!
    @IBOutlet var driverName: UILabel!
    @IBOutlet var carName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layoutIfNeeded()
        driverImageView.layer.cornerRadius = driverImageView.frame.width/2
        driverImageView.clipsToBounds = true
        carImageView.layer.cornerRadius = carImageView.frame.width/2
        carImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
