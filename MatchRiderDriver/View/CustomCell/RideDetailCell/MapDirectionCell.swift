//
//  MapDirectionCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 29/03/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class MapDirectionCell: UITableViewCell {

    @IBOutlet var directionView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        directionView.setCircleView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
