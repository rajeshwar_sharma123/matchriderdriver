//
//  JourneyDetailCell.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 31/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit

class JourneyDetailCell: UITableViewCell {
    @IBOutlet var startLabel: UILabel!

    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    
    @IBOutlet var fareLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
