//
//  ColorPickerView.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 05/06/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import UIKit
protocol ColorPickerViewDelegate {
    func getSelectedColor(dic:NSDictionary)
}

class ColorPickerView: UIView,UIGestureRecognizerDelegate {
    @IBOutlet var contentView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    var colorList: NSArray = []
    var selectedColor: String!
    var delegate: ColorPickerViewDelegate! = nil
    
    override func awakeFromNib() {
        self.setBackgroundColorTransparent()
        
        if let path = Bundle.main.path(forResource: "ColorList", ofType: "plist") {
            if let colorArr = NSArray(contentsOfFile: path) {
                colorList = colorArr
            }
        }
        selectedColor = ""
        collectionView.register(UINib(nibName: "ColorPickerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ColorPickerCollectionCell")
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isDescendant(of: contentView){
            return false
        }
        return true
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    func reloadData(color: String){
        selectedColor = color
        collectionView.reloadData()
    }
    
    
}

extension ColorPickerView: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return colorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorPickerCollectionCell", for: indexPath as IndexPath) as! ColorPickerCollectionCell
        let dic = colorList[indexPath.row] as! NSDictionary
        let colorCode = dic.object(forKey: "Code") as! String
        cell.backgroundColor = UIColor.clear
        cell.layoutIfNeeded()
        cell.colorView.setCircleView()
        cell.tickView.setCircleView()
        if selectedColor == colorCode {
           cell.tickView.isHidden = false
        }else{
            cell.tickView.isHidden = true
        }
        
        cell.colorView.backgroundColor = hexStringToUIColor(hex: colorCode)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 60, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dic = colorList[indexPath.row] as! NSDictionary
        //selectedColor = dic.object(forKey: "Code") as! String
        delegate.getSelectedColor(dic: dic)
        self.removeFromSuperview()
        //collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
